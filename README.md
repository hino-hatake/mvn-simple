# mvn simple (indeed)
Generated with `maven-archetype-simple`, let's see how `mvn release` works with `nexus3` repository.

## How to
**Disclaimer:** The guideline below won't be up-to-date, refer the latest sauce here: https://gitlab.com/hino-hatake/nexus3-eks

### 1. Generate stuff with maven-archetype-simple
```bash
mvn archetype:generate \
  -DgroupId=one.hino \
  -DartifactId=mvn-simple \
  -Dversion=1.0.0 \
  -DarchetypeGroupId=org.apache.maven.archetypes \
  -DarchetypeArtifactId=maven-archetype-simple \
  -DarchetypeVersion=1.4 \
  -DarchetypeCatalog=remote \
  -DinteractiveMode=false
```

You might need to move them out:
```bash
mv mvn-simple/* .
```

### 2. Test the app
Package it:
```bash
mvn clean package
```
Run it:
```bash
java -cp target/mvn-simple-1.0.0.jar one.hino.App
```

### 3. Create nexus user
First, create a role `nx-deploy` with privilege `nx-repository-view-*-*-*` (or more specific to maven: `nx-repository-view-maven2-*-*`), it will be able perform all these actions: `browse`, `read`, `edit`, `add`, and `delete`.

Create an user `deployer` with the newly created role `nx-deploy`, we would have: `deployer` / `<your-mighty-password>`

### 4. Encrypt your password
Sauce: https://maven.apache.org/guides/mini/guide-encryption.html

Run this, or don't input the password as plaintext, just tell maven to prompt for it:
```bash
mvn --encrypt-master-password <your-master-password>
mvn --encrypt-master-password
```

Then create `~/.m2/settings-security.xml` with the content below:
```xlm
<settingsSecurity>
  <master>{encrypted-master-password}</master>
</settingsSecurity>
```

Or you can simply run this:
```bash
cat <<EOF > ~/.m2/settings-security.xml
<settingsSecurity>
  <master>$(mvn --encrypt-master-password <your-master-password>)</master>
</settingsSecurity>
EOF
```

Now encrypt your own password, says, for `deployer` user:
```bash
mvn --encrypt-password <your-mighty-password>
```

Put it in your `~/.m2/settings.xml`, into `<servers>` block:
```xml
<server>
  <id>nexus-releases</id>
  <username>deployer</username>
  <password>{encrypted}</password>
</server>
```

Verify the settings with:
```bash
mvn help:effective-settings
```

### 5. Deploy the artifact
Note that you have a block like this in your `pom.xml`, with the same server id as in your `~/.m2/settings.xml`:
```xml
<distributionManagement>
  <repository>
    <id>nexus-releases</id>
    <name>Nexus3 Test Repository</name>
    <url>https://nexus.hino.one/repository/maven-releases/</url>
  </repository>
</distributionManagement>
```
Now run it:
```bash
mvn clean deploy
mvn clean deploy -DskipTests
```
